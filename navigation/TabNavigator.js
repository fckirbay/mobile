import React, { useState, useEffect } from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Ionicons, FontAwesome5 } from "@expo/vector-icons";
import { Image, Platform } from "react-native";
import Home from "../screens/Home";
import FlipFlop from "../screens/FlipFlop";
import Tasks from "../screens/Tasks";
import Bank from "../screens/Bank";
import i18n from 'i18n-js';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Surveys from "../screens/Surveys";

const Tab = createBottomTabNavigator();

const BottomTabNavigator = ({ navigation }) => {
    const [accessToken, setAccessToken] = useState("");
    const [visible, setVisible] = useState(false);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            AsyncStorage.getItem('access_token')
            .then((value) => {
                if(value != null) {
                 setVisible(true)
                } else {
                 setVisible(false)
                 
                } 
            });
        });
        return () => {
          unsubscribe();
        };
      }, [navigation]);

    return (
        <Tab.Navigator>
            <Tab.Screen
                name={i18n.t('home')}
                component={Home}
                options={{
                    headerShown: false,
                    tabBarActiveTintColor: '#f46346',
                    tabBarIcon: ({ color, size }) => (
                        <Image
                                source={require('../assets/icons/megaphone.png')}
                                style={{ width: 26, height: 26, tintColor: color }}
                            />)
                }}
            />
            
            <Tab.Screen
                name={i18n.t('tasks')}
                component={Tasks}
                listeners={{
                    tabPress: (e) => {
                        if(visible == false) {
                            e.preventDefault();
                            navigation.navigate('Login')
                        }
                    },
                }}
                options={{
                    headerShown: false,
                    tabBarActiveTintColor: '#f46346',
                    tabBarIcon: ({ color, size }) => (
                        <Image
                                source={require('../assets/icons/hand.png')}
                                style={{ width: 26, height: 26, tintColor: color }}
                            />)
                }}
            />
            <Tab.Screen
                name="Higher & Lower"
                component={FlipFlop}
                listeners={{
                    tabPress: (e) => {
                        if(visible == false) {
                            e.preventDefault();
                            navigation.navigate('Login')
                        }
                    },
                }}
                options={{
                    headerShown: false,
                    tabBarActiveTintColor: '#f46346',
                    tabBarIcon: ({ color, size }) => (
                        <Image
                                source={require('../assets/icons/sort-up.png')}
                                style={{ width: 26, height: 26, tintColor: color }}
                            />)
                }}

            />
            {Platform.OS == 'xxx' ? <Tab.Screen
                name={i18n.t('surveys')}
                component={Surveys}
                listeners={{
                    tabPress: (e) => {
                        if(visible == false) {
                            e.preventDefault();
                            navigation.navigate('Login')
                        }
                    },
                }}
                options={{
                    headerShown: false,
                    tabBarActiveTintColor: '#f46346',
                    tabBarIcon: ({ color, size }) => (
                        <Image
                                source={require('../assets/icons/checklist.png')}
                                style={{ width: 26, height: 26, tintColor: color }}
                            />)
                }}
            />:null}
            <Tab.Screen
                name={i18n.t('bank')}
                component={Bank}
                listeners={{
                    tabPress: (e) => {
                        if(visible == false) {
                            e.preventDefault();
                            navigation.navigate('Login')
                        }
                    },
                }}
                options={{
                    headerShown: false,
                    tabBarActiveTintColor: '#f46346',
                    tabBarIcon: ({ color, size }) => (
                        <Image
                                source={require('../assets/icons/bank.png')}
                                style={{ width: 26, height: 26, tintColor: color }}
                            />)
                }}
            />
    
   </Tab.Navigator>

    );
};

export default BottomTabNavigator;