import React, { useEffect, useState, } from 'react';
import { Switch, StatusBar } from 'react-native'
import { createDrawerNavigator, DrawerItem } from "@react-navigation/drawer";

import { Ionicons } from '@expo/vector-icons';

import { NavigationContainer, DarkTheme, DefaultTheme } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack';


import { EventRegister } from 'react-native-event-listeners'
import theme from '../config/theme'
import themeContext from '../config/themeContext';

import TabNavigator from "./TabNavigator";

import Login from '../screens/Login';
import Register from '../screens/Register';
import History from '../screens/History';
import References from '../screens/References';
import Transactions from '../screens/Transactions';
import Help from '../screens/Help';
import TaskDetail from '../screens/TaskDetail';
import PrivacyPolicy from '../screens/PrivacyPolicy';
import TermsConditions from '../screens/TermsConditions';
import FlipFlop from '../screens/FlipFlop';
import GameHistory from '../screens/GameHistory';
import Winners from '../screens/Winners';
import HowToPlay from '../screens/HowToPlay';

import DrawerContent from "./DrawerContent";

const Drawer = createDrawerNavigator();

const Stack = createNativeStackNavigator();


const Navigator = () => {
  const [isEnabled, setIsEnabled] = useState(false);
  
  useEffect(() => {
    let eventListener = EventRegister.addEventListener(
      'themeChange',
      (data) => {
        setIsEnabled(data);
        console.log(data);
      }
    );
    return () => {
      //EventRegister.removeEventListener(eventListener);
    };
  });


  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home" screenOptions={{headerShown: false}}>
        <Stack.Screen name="Home" component={TabNavigator} />
        <Stack.Screen name="Bank" component={TabNavigator} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="History" component={History} />
        <Stack.Screen name="References" component={References} />
        <Stack.Screen name="Transactions" component={Transactions} />
        <Stack.Screen name="Help" component={Help} />
        <Stack.Screen name="Details" component={Login} />
        <Stack.Screen name="TaskDetail" component={TaskDetail} />
        <Stack.Screen name="PrivacyPolicy" component={PrivacyPolicy} />
        <Stack.Screen name="TermsConditions" component={TermsConditions} />
        <Stack.Screen name="FlipFlop" component={FlipFlop} />
        <Stack.Screen name="GameHistory" component={GameHistory} />
        <Stack.Screen name="Winners" component={Winners} />
        <Stack.Screen name="HowToPlay" component={HowToPlay} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default Navigator;