import React, { useState } from "react";
import { View, StyleSheet, Text, ImageBackground, ScrollView, TouchableOpacity, Dimensions, Linking } from "react-native";
import { Icon } from "react-native-elements";
import {decode} from 'html-entities';
import AsyncStorage from '@react-native-async-storage/async-storage';
import i18n from 'i18n-js';

const TaskDetail = ({ route,navigation }) => {
  const [accessToken, setAccessToken] = useState("");
  const { itemId, otherParam } = route.params;

  AsyncStorage.getItem('access_token')
  .then((value) => {
    setAccessToken(value);
  });

    renderDetail = () => {
        return (
         /* <Text style={styles.subDetailText}> - New users only</Text>
            <Text style={styles.subDetailText}> - Register or complete tutorial, if asked so</Text>
            <Text style={styles.subDetailText}> - Dont change between networks</Text>
            <Text style={styles.subDetailText}> - When asked you should 'Allow' the app/game to track your activity. The identifier will be used to grant your credits.</Text>*/
          <View>
            <Text style={styles.descriptionText}>Description</Text>
            <Text style={styles.subDetailText}> - { decode(otherParam.description)}</Text>
            <Text style={styles.descriptionText}>Requirements</Text>
            <Text style={styles.subDetailText}> - { decode(otherParam.requirements)}</Text>        
          </View>
          
        )
      }
      
      renderDescription = () => {
        return (
          <View>
            <Text style={styles.priceText}>{parseInt(otherParam.payout * 100)}<Icon
                  name="star"
                  size={25}
                  color="black"
                /></Text>
            <Text style={styles.detailTextRequirements}>{decode(otherParam.anchor)}</Text>
          </View>
        )
      }

      renderContactHeader = () => {
        return (
          <View style={styles.headerContainer}>
            <View style={styles.coverContainer}>
              <ImageBackground
                source={{uri:otherParam.icon}}
                style={styles.coverImage}
              >
              </ImageBackground>
            </View>
          </View>
        )
      }
    
        return (
          <View style={styles.mainViewStyle}>
            <ScrollView  style={styles.scroll}>
              <View style={[styles.container]}>
                <View style={styles.cardContainer}>
                  {renderContactHeader()}
                </View>
              </View>
              <View style={styles.productRow}>{renderDescription()}</View>
              <View style={styles.productInfo}>{renderDetail()}</View>
            </ScrollView>
            <View style={styles.footer}>
              <TouchableOpacity style={styles.buttonFooter} onPress={() => navigation.goBack()}>
                <Text style={styles.textFooter}>{i18n.t('back')}</Text>
              </TouchableOpacity>
              <View style={styles.borderCenter} />
              <TouchableOpacity style={styles.buttonFooter} onPress={() => accessToken != null ? Linking.openURL(`${otherParam.click_url}` + '1') : navigation.navigate('Login')}>
                <Text style={styles.textFooter}>{i18n.t('start_task')}</Text>
              </TouchableOpacity>
              
            </View>
          </View>
        )
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
  },
  cardContainer: {
    flex: 1,
  },
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  coverContainer: {
    position: 'relative',
  },
  coverImage: {
    height: Dimensions.get('window').width * (3 / 4),
    width: Dimensions.get('window').width,
  },
  /*  coverImage: {
    width: Dimensions.get('window').width,
    aspectRatio: 1
  },
  */
  headerContainer: {
    alignItems: 'center',
    backgroundColor: '#FFF',
  },
  scroll: {
    backgroundColor: '#FFF',
    flex: 1,
    marginBottom: 55,
  },
  productRow: {
    marginLeft: 25,
    marginTop: 25,
    marginRight: 25,
  },
  productInfo: {
    marginTop:10,
    margin: 20,
  },
  mainViewStyle: {
    flex: 1,
    flexDirection: 'column',
    flexGrow: 1,
  },
  coverMetaContainer: {
    alignItems: 'flex-end',
    flex: 1,
    justifyContent: 'flex-end',
  },
  footer: {
    alignItems: 'center',
    backgroundColor: '#e36449',
    bottom: 0,
    flex: 0.1,
    flexDirection: 'row',
    height: 65,
    left: 0,
    position: 'absolute',
    right: 0,
  },
  buttonFooter: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  navigatorButton: {
    alignItems: 'flex-start',
    flex: 1,
    justifyContent: 'center',
  },
  navigatorText: {
    alignItems: 'flex-start',
    color: 'green',
    fontSize: 16,
    fontWeight: 'bold',
    justifyContent: 'center',
  },
  borderCenter: {
    borderColor: '#FFA890',
    borderWidth: 0.5,
    height: 55,
  },
  textFooter: {
    alignItems: 'center',
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
  },
  priceText: {
    color: 'black',
    fontSize: 36,
    fontWeight: '400',
    letterSpacing: 1,
    marginBottom: 5,
  },
  detailText: {
    color: 'black',
    fontSize: 22,
    fontWeight: '600',
    letterSpacing: 0.5,
    marginBottom: 4,
  },
  detailTextRequirements: {
    color: 'black',
    fontSize: 22,
    fontWeight: '600',
    letterSpacing: 0.5,
    marginBottom: 4,
  },
  subDetailText: {
    color: 'black',
    fontSize: 16,
    fontWeight: '100',
    letterSpacing: 0.5,
    lineHeight: 28,
  },
  descriptionText: {
    color: 'gray',
    fontSize: 16,
    fontWeight: '400',
    letterSpacing: 1,
    marginBottom: 4,
    marginTop: 10,
  },
});

export default TaskDetail;
