import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text, StatusBar, SafeAreaView, ScrollView } from "react-native";
import { ListItem, Icon, Button } from "react-native-elements";
import history from "../apis/History";
import moment from 'moment';
import i18n from 'i18n-js';

const References = ({ navigation }) => {
    const [historyList, sethistoryList] = useState([]);
    const [balanceType, setBalanceType] = useState('star');

    useEffect(() => {
      const unsubscribe = navigation.addListener('focus', () => {
        async function getHistory() {
            try {
            const params = { 
                ref : '1',
            };   
              const { historyData } = await history(params);
              sethistoryList(historyData);
          } catch (error) {
              console.log('HistoryScreen getHistory() Error', error);
          }
        }
        getHistory();
      });
      return () => {
        unsubscribe();
      };
    }, [navigation]);

    const convertBalance = (type) => {
      setBalanceType(type);
    }
  
  return (
    <SafeAreaView>
      <StatusBar style="auto" />
      <View style={styles.container}>
          <Text style={styles.infoText}>{i18n.t('references')}</Text>
          {balanceType == 'star' ? <Button
            onPress={() => convertBalance('usd')}
            buttonStyle={{marginRight: 10,alignItems: 'center',
            justifyContent: 'center',
            paddingVertical: 0,
            paddingHorizontal: 12,}}
            title={i18n.t('convert_usd')}
            type="clear"
          /> : <Button
          onPress={() => convertBalance('star')}
          buttonStyle={{marginRight: 10,alignItems: 'center',
          justifyContent: 'center',
          paddingVertical: 0,
          paddingHorizontal: 12,}}
          title={i18n.t('convert_star')}
          type="clear"
        />}
        </View>
        <ScrollView>
        {historyList.map((element, index) => (
          <ListItem key={index} bottomDivider>
            <ListItem.Content>
              <ListItem.Title>{element.offer_title}</ListItem.Title>
              <ListItem.Subtitle>{i18n.t('reference')} { element.users.username} / {moment(element.complete_date, 'YYYY-MM-DD HH:mm:ss').format('DD MMM YYYY HH:mm')} </ListItem.Subtitle>

            </ListItem.Content>
            <ListItem.Title style={{ fontWeight: "bold" }}>
            {balanceType == 'usd' ? '$' : ''}{ balanceType == 'star' ? parseInt(element.point_value) : (element.point_value / 100).toFixed(2) }{balanceType == 'star'?<Icon
                  name="star"
                  size={15}
                  color="black"
                  iconStyle={{top: 2}}
                />:''}
            </ListItem.Title>
          </ListItem>
        ))}
        {
          historyList.length >= 0 && <Text style={styles.infoText}>{i18n.t('no_record')}</Text>
        }
 
      </ScrollView>

    </SafeAreaView>

  );
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
  },
  container: {
    paddingTop: 20,
    paddingBottom: 12,
    backgroundColor: "#F4F5F4",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  infoText: {
    fontSize: 16,
    color: "black",
    fontWeight: "bold",
    marginLeft: 20,
    marginRight: 20
  },
});

export default References;
