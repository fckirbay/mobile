import React, { useState, useEffect } from "react";
import { View, StyleSheet, Text, StatusBar } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import Accordion from '../components/Accordion';
import faq from "../apis/Faq";
import i18n from 'i18n-js';

const Help = ({ navigation }) => {

  const [faqs, setFaqs] = useState([]);
  
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      async function getFaqs() {
        try {
            const { faqData } = await faq();
            setFaqs(faqData);
        } catch (error) {
            console.log('FaqScreen getFaqs() Error', error);
        }
      }
      getFaqs();
    });
    return () => {
      unsubscribe();
    };
  }, [navigation]);


  return (
    <View>
      <View style={styles.container}>
        <Text style={styles.infoText}>{i18n.t('help')}</Text>
      </View>
      <ScrollView>
      {faqs.map((l, i) => (
        <Accordion title={l.question} content={l.answer} />
      ))}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
  },
  container: {
    paddingTop: 20,
    paddingBottom: 12,
    backgroundColor: "#F4F5F4",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  infoText: {
    fontSize: 16,
    color: "black",
    fontWeight: "bold",
    marginLeft: 20,
    marginRight: 20,
  },
});

export default Help;
