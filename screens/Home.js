import React, { useState, useEffect, useContext } from "react";
import {
  View,
  StyleSheet,
  Text,
  ScrollView,
  StatusBar,
  Image,
  Dimensions,
  SafeAreaView,
} from "react-native";
import { Avatar, Divider, Icon, Card } from "react-native-elements";
import {decode} from 'html-entities';
import themeContext from "../config/themeContext";
import generalstats from "../apis/GeneralStats";
import sliderimages from "../apis/SliderImages";
import tasks from "../apis/Tasks";
import i18n from 'i18n-js';
import { TouchableOpacity } from "react-native-gesture-handler";

const { width, height } = Dimensions.get("window");

//API call
const Home = ({ navigation }) => {
  const theme = useContext(themeContext);
  const [stats, setStats] = useState([]);
  const [sliderImage, setSliderImage] = useState([]);
  const [taskList, setTaskList] = useState([]);

  //setTimeout(() => {this.scrollView.scrollTo({x: -30}) }, 1) // scroll view position fix

  useEffect(() => {
    async function getGeneralStats() {
      try {
          const { generalStatsData } = await generalstats();
          setStats(generalStatsData)
      } catch (error) {
          console.log('HomeScreen getGeneralStats() Error', error);
      }
    }
    async function getSliderImages() {
      const params = {
        lang:  "TR"
       };
      try {
          const { sliderImagesData } = await sliderimages(params);
          setSliderImage(sliderImagesData)
      } catch (error) {
          console.log('HomeScreen getSliderImages() Error', error);
      }
    }
    async function getTasks() {
      try {
          const { tasksData } = await tasks();
          const limitedTasks = tasksData.slice(0, 10);
          setTaskList(limitedTasks)
      } catch (error) {
          console.log('TasksScreen getGeneralStats() Error', error);
      }
    }
      getGeneralStats();
      getSliderImages();
      getTasks();
  }, []);

  return (
    <SafeAreaView style={{ backgroundColor: theme.backColor }}>
      <StatusBar style="auto"  />
      <ScrollView 
        style={styles.container}
        //pagingEnabled={true}
        horizontal= {true}
        decelerationRate={0}
        snapToInterval={width - 60}
        snapToAlignment={"center"}
        contentInset={{
          top: 0,
          left: 30,
          bottom: 0,
          right: 30,
        }}>
         {sliderImage && sliderImage.map((slider, i) => (
            <Image
              key={i}
              source={{uri: slider.photo}}
              style={styles.sliderView}
            />
         )
        )}
      </ScrollView>
      <View style={styles.content}>
        <View style={styles.textWrapper}>
          <Text style={styles.statText}>{stats ? stats.total_tasks : 0}</Text>
          <Text style={styles.statTextInfo}>{i18n.t('total_task')}</Text>
        </View>
        <Divider orientation="vertical" width={1} />
        <View style={styles.textWrapper}>
          <Text style={styles.statText}>{stats ? stats.completed : 0}</Text>
          <Text style={styles.statTextInfo}>{i18n.t('completed')}</Text>
        </View>
        <Divider orientation="vertical" width={1} />
        <View style={styles.textWrapper}>
          <View style={styles.subText}>
            <Text style={styles.statText}>{parseInt(stats ? stats.rewarded : 0)}<Icon
                  name="star"
                  size={15}
                  color="black"
                /></Text>
          </View>
          <Text style={styles.statTextInfo}>{i18n.t('rewarded')}</Text>
        </View>
      </View>
      <ScrollView style={{ height: height - ((width - (width * 0.2)) / 1.5) - (height * 0.20), top: 10}}>
          {taskList.map((task, i) => (
            <TouchableOpacity key={'task'+i} onPress={() => navigation.navigate('TaskDetail', {
              itemId: i,
              otherParam: task
            })}>
              <View style={styles.taskView} key={"task"+i}  onPress={() => navigation.navigate('TaskDetail',{
                itemId: i,
                otherParam: task
              })}>
                <Image
                  key={i}
                  source={{uri: task.icon}}
                  style={styles.taskImage}
                />
                <View style={styles.anchorTextView}>
                  <Text style={styles.anchorTextStyle}>{decode(task.anchor)}</Text>
                </View>
                <View style={styles.payoutTextView}>
                  <Text style={styles.payoutTextStyle}>{parseInt(task.payout * 50).toString()}
                    <Icon
                      name="star"
                      size={15}
                      color="black"
                      iconStyle={{top: 2}}
                    />
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          )
          )}
        </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  subText: {
    flexDirection: "row",
    justifyContent: "center",
  },
  wrapper: {
    justifyContent: "center",
    alignItems: "center",
    marginVertical: 30,
  },
  textWrapper: {
    width: '33.33%',
    marginTop: 10,
    marginBottom: 10,
  },
  statText: {
    fontSize: 22,
    fontWeight: "bold",
    alignSelf: "center",
    color: "#000000"
  },
  subStatText: {
    fontSize: 12,
    fontWeight: "bold",
    alignSelf: "center",
    marginTop: 10,
  },
  statTextInfo: {
    fontSize: 14,
    lineHeight: 30,
    alignSelf: 'center',
    color: "#2A6874"
  },
  content: {
    flexDirection: "row",
    marginTop: 0,
    marginBottom: 0,
    backgroundColor: '#e6e6e6',
    borderRadius: 10,
    marginLeft: 10,
    marginRight: 10,
    height: '10%',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center'
  },
  sliderView: {
    marginTop: 10,
    backgroundColor: 'white',
    width: width - (width * 0.2),
    margin: 10,
    height: (width - (width * 0.2)) / 1.5 ,
    borderRadius: 10,
    resizeMode:'cover'
    //paddingHorizontal : 30
  },
  taskView: {
    marginTop: 0,
    backgroundColor: '#e6e6e6',
    width: width - 20,
    margin: 10,
    marginLeft: 10,
    height: 50,
    borderRadius: 10,
    flexDirection: 'row'
  },
  taskImage:{
    width: "15%", 
    height: 50, 
    borderTopLeftRadius: 10, 
    borderBottomLeftRadius: 10
  },
  anchorTextView:{
    width: "65%", 
    left: "15%", 
    justifyContent: 'center'
  },
  anchorTextStyle:{
    color: "black", 
    fontSize: 16,     
  },
  payoutTextView:{
    alignItems: 'center', 
    alignSelf: 'center', 
    width: "20%", 
  },
  payoutTextStyle:{
    color: "black", 
    fontSize: 16, 
    fontWeight: "bold"
  }
});

export default Home;
