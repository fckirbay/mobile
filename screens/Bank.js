import React, { useState, useEffect } from "react";
import { View, StyleSheet, Text, ScrollView, StatusBar } from "react-native";
import { Avatar, ListItem, Icon, Button, BottomSheet } from "react-native-elements";
import { SafeAreaView } from "react-native-safe-area-context";
import AsyncStorage from '@react-native-async-storage/async-storage';
import jwt_decode from "jwt-decode";
import i18n from 'i18n-js';
import userInfo from "../apis/UserInfo";


const Bank = ({ navigation }) => {
  const [accessToken, setAccessToken] = useState("");
  const [username, setUsername] = useState("");
  const [userId, setUserId] = useState("");
  const [user, setUser] = useState([]);
  const [balanceType, setBalanceType] = useState('star');
  const [showPaymentMethods, setShowPaymentMethods] = useState(false);
  const list = [
    { title: 'Cryptocurrency' },
    { title: 'Paypal' },
    { title: 'Papara' },
    {
      title: 'Cancel',
      containerStyle: { backgroundColor: 'red' },
      titleStyle: { color: 'white' },
      onPress: () => setShowPaymentMethods(false),
    },
  ];
    AsyncStorage.getItem('access_token')
      .then((value) => {
        const decoded = jwt_decode(value);
        setAccessToken(value);
        setUsername(decoded.username);
        setUserId(decoded.id);
    });

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
 
      async function getUserInfo() {
        try {
            const { userData } = await userInfo();
            setUser(userData);
        } catch (error) {
            console.log('TasksScreen getGeneralStats() Error', error);
        }
      }
      getUserInfo();
    });
    return () => {
      unsubscribe();
    };
  }, [navigation]);

  const logout = () => {
    setAccessToken(null);
    AsyncStorage.removeItem("access_token");
    navigation.reset({
      index: 0,
      routes: [{name: 'Home'}],
    });
    //navigation.navigate('Login');
  }
  
  const convertBalance = (type) => {
    setBalanceType(type);
  }

  const withdrawMoney = () => {
    /*if(user.balance == 0) {
      alert("Insufficiend balance!");
    }*/
    setShowPaymentMethods(true);
  }

    return (
      <SafeAreaView>
      <StatusBar style="auto" />
      <ScrollView style={styles.scroll}>
        <View style={styles.userRow}>
          <View style={styles.userImage}>
            <Avatar
              rounded
              size="large"
              source={require('../assets/img/user-profile.png')}
            />
          </View>
          <View>
            <Text style={{ fontSize: 16 }}>{username}</Text>
            <Text
              style={{
                color: "gray",
                fontSize: 16,
              }}
            >
              {i18n.t('user_id')} {userId}
            </Text>
          </View>
        </View>
        <View style={styles.container}>
          <Text style={styles.infoText}>{i18n.t('balance')}</Text>
          {balanceType == 'star' ? <Button
            onPress={() => convertBalance('usd')}
            buttonStyle={{marginRight: 10,alignItems: 'center',
            justifyContent: 'center',
            paddingVertical: 0,
            paddingHorizontal: 12,}}
            title={i18n.t('convert_usd')}
            type="clear"
          /> : <Button
          onPress={() => convertBalance('star')}
          buttonStyle={{marginRight: 10,alignItems: 'center',
          justifyContent: 'center',
          paddingVertical: 0,
          paddingHorizontal: 12,}}
          title={i18n.t('convert_star')}
          type="clear"
        />}
        </View>
        <View>
          <ListItem containerStyle={styles.listItemContainer}>
            <Icon
              size={24}
              color="grey"
              type="material-community"
              name="safe"
            />
            <ListItem.Content style={{ width: "75%", height: 100 }}>
              <ListItem.Title>{i18n.t('total_earnings')}</ListItem.Title>
            </ListItem.Content>
            <ListItem.Title style={{ fontWeight: "bold" }}>
            {balanceType == 'usd' ? '$' : ''}{user ? balanceType == 'star' ? parseInt(user.earnings) : (user.earnings / 100).toFixed(2) : 0}{balanceType == 'star'?<Icon
                  name="star"
                  size={15}
                  color="black"
                  iconStyle={{top: 2}}
                />:''}
            </ListItem.Title>
          </ListItem>

          <ListItem containerStyle={styles.listItemContainer}>
            <Icon size={24} color="grey" type="ionicon" name="wallet-outline" />
            <ListItem.Content style={{ width: "75%", height: 100 }}>
              <ListItem.Title>{i18n.t('current_balance')}</ListItem.Title>
            </ListItem.Content>
            <ListItem.Title style={{ fontWeight: "bold" }}>
            {balanceType == 'usd' ? '$' : ''}{user ? balanceType == 'star' ? parseInt(user.balance) : (user.balance / 100).toFixed(2) : 0}{balanceType == 'star'?<Icon
                  name="star"
                  size={15}
                  color="black"
                  iconStyle={{top: 2}}
                />:''}
            </ListItem.Title>
          </ListItem>
          <ListItem containerStyle={styles.listItemContainer}>
            <Icon
              size={24}
              color="grey"
              type="material-community"
              name="bank-transfer"
            />
            <ListItem.Content style={{ width: "75%", height: 100 }}>
              <ListItem.Title>{i18n.t('withdrawable_balance')}</ListItem.Title>
            </ListItem.Content>
            <ListItem.Title style={{ fontWeight: "bold" }}>
            {balanceType == 'usd' ? '$' : ''}{user ? balanceType == 'star' ? parseInt(user.withdrawable_balance) : (user.withdrawable_balance / 100).toFixed(2) : 0}{balanceType == 'star'?<Icon
                  name="star"
                  size={15}
                  color="black"
                  iconStyle={{top: 2}}
                />:''}
            </ListItem.Title>
          </ListItem>
          <ListItem containerStyle={styles.listItemContainer}>
            <Icon
              size={24}
              color="grey"
              type="entypo"
              name="slideshare"
            />
            <ListItem.Content style={{ width: "75%", height: 100 }}>
              <ListItem.Title>{i18n.t('reference_earnings')}</ListItem.Title>
            </ListItem.Content>
            <ListItem.Title style={{ fontWeight: "bold" }}>
            {balanceType == 'usd' ? '$' : ''}{user ? balanceType == 'star' ? parseInt(user.ref_earnings) : (user.ref_earnings / 100).toFixed(2) : 0}{balanceType == 'star'?<Icon
                  name="star"
                  size={15}
                  color="black"
                  iconStyle={{top: 2}}
                />:''}
            </ListItem.Title>
          </ListItem>
          <ListItem containerStyle={styles.listItemContainer}>
            <ListItem.Content
              style={{ width: "75%", height: 100, alignItems: "center" }}
            >
              <ListItem.Title>
                <Button
                  onPress={() => withdrawMoney()}
                  title={i18n.t('withdraw_money')}
                  buttonStyle={{ backgroundColor: "green" }}
                />
              </ListItem.Title>
            </ListItem.Content>
          </ListItem>
        </View>
        <View style={styles.container}>
          <Text style={styles.infoText}>More</Text>
        </View>
        <View>
          <ListItem bottomDivider onPress={() => navigation.navigate('History')}>
            <Icon size={24} color="grey" type="font-awesome-5" name="history" />
            <ListItem.Content>
              <ListItem.Title>{i18n.t('history')}</ListItem.Title>
            </ListItem.Content>
            <ListItem.Chevron />
            
          </ListItem>
          <ListItem bottomDivider onPress={() => navigation.navigate('References')}>
            <Icon size={24} color="grey" type="font-awesome-5" name="hand-holding-usd" />
            <ListItem.Content>
              <ListItem.Title>{i18n.t('referral_earnings')}</ListItem.Title>
            </ListItem.Content>
            <ListItem.Chevron />
            
          </ListItem>
          <ListItem bottomDivider onPress={() => navigation.navigate('Transactions')}>
            <Icon
              size={24}
              color="grey"
              type="ionicon"
              name="ios-list-outline"
            />
            <ListItem.Content>
              <ListItem.Title>{i18n.t('transactions')}</ListItem.Title>
            </ListItem.Content>
            <ListItem.Chevron />
          </ListItem>

          <ListItem bottomDivider onPress={() => navigation.navigate('Help')}>
            <Icon size={24} color="grey" type="ionicon" name="help" />
            <ListItem.Content>
              <ListItem.Title>{i18n.t('help')}</ListItem.Title>
            </ListItem.Content>
            <ListItem.Chevron />
          </ListItem>
          <ListItem bottomDivider onPress={() => navigation.navigate('TermsConditions')}>
            <Icon size={24} color="grey" type="antdesign" name="book" />
            <ListItem.Content>
              <ListItem.Title>{i18n.t('terms_conditions')}</ListItem.Title>
            </ListItem.Content>
            <ListItem.Chevron />
          </ListItem>
          <ListItem bottomDivider onPress={() => navigation.navigate('PrivacyPolicy')}>
            <Icon size={24} color="grey" type="material" name="policy" />
            <ListItem.Content>
              <ListItem.Title>{i18n.t('privacy_policy')}</ListItem.Title>
            </ListItem.Content>
            <ListItem.Chevron />
          </ListItem>
          <ListItem bottomDivider onPress={() => logout()}>
            <Icon size={24} color="grey" type="antdesign" name="logout" />
            <ListItem.Content>
              <ListItem.Title>{i18n.t('logout')}</ListItem.Title>
            </ListItem.Content>
          </ListItem>
          
        </View>
      </ScrollView>
      <BottomSheet
        isVisible={showPaymentMethods}
        containerStyle={{ backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)' }}
      >
        {list.map((l, i) => (
          <ListItem key={i} containerStyle={l.containerStyle} onPress={l.onPress}>
            <ListItem.Content>
              <ListItem.Title style={l.titleStyle}>{l.title}</ListItem.Title>
            </ListItem.Content>
          </ListItem>
        ))}
      </BottomSheet>
    </SafeAreaView>
    )
  

};

/*
<ListItem bottomDivider>
  <Icon size={24} color="grey" type="antdesign" name="delete" />
  <ListItem.Content>
    <ListItem.Title>Delete Account</ListItem.Title>
  </ListItem.Content>
  <ListItem.Chevron />
</ListItem>
*/

const styles = StyleSheet.create({
  scroll: {
    backgroundColor: "white",
  },
  userRow: {
    alignItems: "center",
    flexDirection: "row",
    paddingBottom: 8,
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 6,
    height: 120,
  },
  userImage: {
    marginRight: 12,
  },
  listItemContainer: {
    height: 55,
    borderWidth: 0.5,
    borderColor: "#ECECEC",
  },
  container: {
    paddingTop: 20,
    paddingBottom: 12,
    backgroundColor: "#F4F5F4",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  infoText: {
    fontSize: 16,
    color: "gray",
    fontWeight: "500",
    marginLeft: 20,
    marginRight: 20
  },
  infoTextRight: {
    fontSize: 16,
    marginRight: 20,
    color: "gray",
    fontWeight: "500",
  },
});

export default Bank;
