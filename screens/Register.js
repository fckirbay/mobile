import React, { useState } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  ImageBackground,
  Text,
  TextInput,
  TouchableOpacity
} from "react-native";
import EvilIconsIcon from "react-native-vector-icons/EvilIcons";
import AsyncStorage from '@react-native-async-storage/async-storage';
import i18n from 'i18n-js';
import register from "../apis/Register";

const Register = ({navigation}) => {

  const [inputUsername, setusername] = useState('');
  const [inputPassword, setpassword] = useState('');
  const [inputConfirmPassword, setconfirmpassword] = useState('');
  var username = inputUsername;
  var password = inputPassword;
  var confirmPassword = inputConfirmPassword;

  async function onRegister(username, password, confirmPassword) {
    if(username == '' || password == '' || confirmPassword == '') {
      alert(i18n.t('required_fields'));
    } else if(password != confirmPassword) {
      alert(i18n.t('pwd_match'));
    } else {
      const params = {
        username: username,
        password: password
       };
      try {
          const { registerData } = await register(params);
          if(registerData.errorCode) {
            if(registerData.errorCode == '104'){
              alert(i18n.t('errorCode104'))
            } else if(registerData.errorCode == '102') {
              alert(i18n.t('errorCode102'))
            } else {
              alert(registerData.errorCode)
            }
          } else {
            AsyncStorage.setItem("access_token", registerData.accessToken);
            AsyncStorage.setItem("refresh_token", registerData.refreshToken);
            alert(i18n.t('register_success'));
            navigation.navigate('Login')
          }
      } catch (error) {
          console.log('HomeScreen getSliderImages() Error', error);
      }
    }
  }
  
  return (
    <View style={styles.root}>
      <View style={styles.background}>
        <ImageBackground
          style={styles.rect}
          imageStyle={styles.rect_imageStyle}
          source={require("../assets/img/login_bg.png")}
        >
          <View style={styles.logoColumn}>
            <View style={styles.form}>
              <View style={styles.usernameColumn}>
                <View style={styles.username}>
                  <EvilIconsIcon
                    name="user"
                    style={styles.icon22}
                  ></EvilIconsIcon>
                  <TextInput
                    placeholder={i18n.t('username')}
                    placeholderTextColor="rgba(255,255,255,1)"
                    secureTextEntry={false}
                    style={styles.usernameInput}
                    value={username}
                    onChangeText={(username) => {
                      setusername(username)
                    }}
                  ></TextInput>
                </View>
                <View style={styles.password}>
                  <EvilIconsIcon
                    name="lock"
                    style={styles.icon2}
                  ></EvilIconsIcon>
                  <TextInput
                    placeholder={i18n.t('password')}
                    placeholderTextColor="rgba(255,255,255,1)"
                    secureTextEntry={true}
                    style={styles.passwordInput}
                    value={password}
                    onChangeText={(password) => {
                      setpassword(password)
                    }}
                  ></TextInput>
                </View>
                <View style={styles.password}>
                  <EvilIconsIcon
                    name="lock"
                    style={styles.icon2}
                  ></EvilIconsIcon>
                  <TextInput
                    placeholder={i18n.t('confirm_password')}
                    placeholderTextColor="rgba(255,255,255,1)"
                    secureTextEntry={true}
                    style={styles.passwordInput}
                    value={confirmPassword}
                    onChangeText={(confirmPassword) => {
                      setconfirmpassword(confirmPassword)
                    }}
                  ></TextInput>
                </View>
              </View>
              <View style={styles.usernameColumnFiller}></View>
            </View>
            <TouchableOpacity
                onPress={() => onRegister(username, password, confirmPassword) }
                style={styles.button}
              >
                <Text style={styles.text2}>{i18n.t('register')}</Text>
           </TouchableOpacity>
          </View>
          <View style={styles.logoColumnFiller}></View>
          <View style={styles.footerTexts}>
            <TouchableOpacity
              onPress={() => navigation.navigate("Login")}
              style={styles.button2}
            >
              <View style={styles.createAccountFiller}></View>
              <Text style={styles.createAccount}>{i18n.t('already_have_account')}</Text>
            </TouchableOpacity>
            <View style={styles.button2Filler}></View>
            <Text style={styles.needHelp}>{i18n.t('need_help')}</Text>
          </View>
        </ImageBackground>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "rgb(255,255,255)"
  },
  background: {
    flex: 1
  },
  rect: {
    flex: 1
  },
  rect_imageStyle: {},
  logo: {
    width: 102,
    height: 111,
    alignSelf: "center"
  },
  endWrapperFiller: {
    flex: 1
  },
  text3: {
    color: "rgba(255,255,255,1)",
    fontSize: 30,
    marginBottom: 4
  },
  rect7: {
    height: 8,
    backgroundColor: "#25cdec"
  },
  text3Column: {
    marginBottom: 6,
    marginLeft: 2,
    marginRight: 3
  },
  form: {
    height: 230,
    marginTop: 120
  },
  username: {
    height: 59,
    backgroundColor: "rgba(251,247,247,0.25)",
    borderRadius: 5,
    flexDirection: "row"
  },
  icon22: {
    color: "rgba(255,255,255,1)",
    fontSize: 30,
    marginLeft: 20,
    alignSelf: "center"
  },
  usernameInput: {
    height: 30,
    color: "rgba(255,255,255,1)",
    flex: 1,
    marginRight: 11,
    marginLeft: 11,
    marginTop: 14
  },
  password: {
    height: 59,
    backgroundColor: "rgba(253,251,251,0.25)",
    borderRadius: 5,
    flexDirection: "row",
    marginTop: 27
  },
  icon2: {
    color: "rgba(255,255,255,1)",
    fontSize: 33,
    marginLeft: 20,
    alignSelf: "center"
  },
  passwordInput: {
    height: 30,
    color: "rgba(255,255,255,1)",
    flex: 1,
    marginRight: 17,
    marginLeft: 8,
    marginTop: 14
  },
  usernameColumn: {},
  usernameColumnFiller: {
    flex: 1
  },
  button: {
    height: 59,
    backgroundColor: "rgba(31,178,204,1)",
    borderRadius: 5,
    justifyContent: "center",
    marginTop: 25,
  },
  text2: {
    color: "rgba(255,255,255,1)",
    alignSelf: "center"
  },
  logoColumn: {
    marginTop: 80,
    marginLeft: 41,
    marginRight: 41
  },
  logoColumnFiller: {
    flex: 1
  },
  footerTexts: {
    height: 14,
    flexDirection: "row",
    marginBottom: 36,
    marginLeft: 37,
    marginRight: 36
  },
  button2: {
    width: 150,
    height: 14,
    alignSelf: "flex-end"
  },
  createAccountFiller: {
    flex: 1
  },
  createAccount: {
    color: "rgba(255,255,255,0.5)"
  },
  button2Filler: {
    flex: 1,
    flexDirection: "row"
  },
  needHelp: {
    color: "rgba(255,255,255,0.5)",
    alignSelf: "flex-end",
    marginRight: -1
  }
});

export default Register;
