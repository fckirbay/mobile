import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text, StatusBar, SafeAreaView, ScrollView } from "react-native";
import { ListItem, Icon, Button } from "react-native-elements";
import transaction from "../apis/Transaction";
import moment from 'moment';
import i18n from 'i18n-js';

const Transactions = ({ navigation }) => {
  const [transactionList, setTransactionList] = useState([]);
  const [balanceType, setBalanceType] = useState('star');

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      async function getTransactions() {
        try {
            const { transactionData } = await transaction();
            setTransactionList(transactionData);
        } catch (error) {
            console.log('TransactionsScreen getTransactions() Error', error);
        }
      }
      getTransactions();
    });
    return () => {
      unsubscribe();
    };
  }, [navigation]);

  const renderPaymentStatus = (paymentStatus,amount) => {

    if(balanceType == 'usd') {
      amount = (amount / 100).toFixed(2);
    } else {
      amount = parseInt(amount);
    }
      if(paymentStatus == 0) {
        return (
          <ListItem.Title style={{ fontWeight: "bold", color: 'grey' }}>
                {balanceType == 'usd' ? '$':''}{amount}{balanceType == 'star' ? <Icon
                  name="star"
                  size={15}
                  color="grey"
                  iconStyle={{top: 2}}
                />:""} <Icon size={12} color="grey" type="font-awesome-5" name="history" iconStyle={{ top: 1}} />
            </ListItem.Title>
        )
      } else if(paymentStatus == 1) {
        return (
          <ListItem.Title style={{ fontWeight: "bold", color: 'green' }}>
              {balanceType == 'usd' ? '$':''}{amount}{balanceType == 'star' ? <Icon
                  name="star"
                  size={15}
                  color="green"
                  iconStyle={{top: 2}}
                />:""}
              <Icon size={14} color="green" type="entypo" name="check" iconStyle={{ top: 3}} />
          </ListItem.Title>  
        )
      } else if(paymentStatus == 2) {
        return (
          <ListItem.Title style={{ fontWeight: "bold", color: 'red' }}>
             {balanceType == 'usd' ? '$':''}{amount}{balanceType == 'star' ? <Icon
                  name="star"
                  size={15}
                  color="red"
                  iconStyle={{top: 2}}
                />:""}
             <Icon size={14} color="red" type="font-awesome-5" name="times" iconStyle={{ top: 3}} />
          </ListItem.Title>  
          )
      }
  }

  const convertBalance = (type) => {
    setBalanceType(type);
  }

  return (
    <SafeAreaView>
      <StatusBar style="auto" />
      <View style={styles.container}>
          <Text style={styles.infoText}>{i18n.t('transactions')}</Text>
          {balanceType == 'star' ? <Button
            onPress={() => convertBalance('usd')}
            buttonStyle={{marginRight: 10,alignItems: 'center',
            justifyContent: 'center',
            paddingVertical: 0,
            paddingHorizontal: 12,}}
            title={i18n.t('convert_usd')}
            type="clear"
          /> : <Button
          onPress={() => convertBalance('star')}
          buttonStyle={{marginRight: 10,alignItems: 'center',
          justifyContent: 'center',
          paddingVertical: 0,
          paddingHorizontal: 12,}}
          title={i18n.t('convert_star')}
          type="clear"
        />}
        </View>
      <ScrollView>
        {transactionList.map((element, index) => (
          <ListItem key={index} bottomDivider>
            <ListItem.Content>
              <ListItem.Title>{i18n.t('payment_type')}: {element.type == 1 ? 'Cryptocurrency' : 'Papara'}</ListItem.Title>
              <ListItem.Subtitle>{i18n.t('created_date')} {moment(element.created_date, 'YYYY-MM-DD HH:mm:ss').format('DD MMM YYYY HH:mm')} </ListItem.Subtitle>
              {element.confirm_date ? <ListItem.Subtitle>{i18n.t('confirm_date')} {moment(element.confirm_date, 'YYYY-MM-DD HH:mm:ss').format('DD MMM YYYY HH:mm')}</ListItem.Subtitle> : ''}
            </ListItem.Content>
             {renderPaymentStatus(element.payment_status,element.amount)}
          </ListItem>
        ))}
        {
          transactionList.length >= 0 && <Text style={styles.infoText}>{i18n.t('no_record')}</Text>
        }
      </ScrollView>

    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
  },
  listItemContainer: {
    height: 55,
    borderWidth: 0.5,
    borderColor: "#ECECEC",
  },
  container: {
    paddingTop: 20,
    paddingBottom: 12,
    backgroundColor: "#F4F5F4",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  infoText: {
    fontSize: 16,
    color: "black",
    fontWeight: "bold",
    marginLeft: 20,
    marginRight: 20
  },
});

export default Transactions;
