import React, { useState, useEffect, useContext } from "react";
import { View, StyleSheet, Text, FlatList, StatusBar, Platform } from "react-native";
import { ListItem, Avatar, Button, Icon } from "react-native-elements";
import { SafeAreaView } from "react-native-safe-area-context";
import {decode} from 'html-entities';
import tasks from "../apis/Tasks";
import {AyetOfferwall}  from 'ayetsdk';

const Surveys = ({ navigation }) => {
  const onClose = () => {
    console.log("close")
    // code you want to execute on offerwall close button pressed  
  }
  return (
 
    <AyetOfferwall 
      userId = '213586'
      adslotId = '6679'
      onClose = { onClose } 
      />
  );
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
  },
});

export default Surveys;
