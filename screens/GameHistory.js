import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text, StatusBar, SafeAreaView, ScrollView } from "react-native";
import { ListItem, Icon, Button } from "react-native-elements";
import i18n from 'i18n-js';
import history from "../apis/History";
import moment from 'moment';

import games from "../apis/Games";

const GameHistory = ({ navigation }) => {
    const [game, setGame] = useState([]);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
          async function getGames() {
            try {
                const { gameData } = await games({is_completed: 1});
                if(gameData.length > 0) {
                  setGame(gameData);
                }
            } catch (error) {
                console.log('Games getGames() Error', error);
            }
          }
          getGames();
        });
        return () => {
          unsubscribe();
        };
      }, [navigation]);

  
  return (
    <SafeAreaView>
      <StatusBar style="auto" />
      <View style={styles.container}>
          <Text style={styles.infoText}>Playing Time</Text>
          <Text style={{marginRight: 10,alignItems: 'center',
            justifyContent: 'center',
            paddingVertical: 0,
            paddingHorizontal: 12, fontWeight: "bold"}}>Score</Text>
 
        </View>
      <ScrollView>
        {game.map((element, index) => (
          <ListItem key={index} bottomDivider>
            <ListItem.Content>
              <ListItem.Subtitle style= {{ paddingHorizontal: 12 }}>{moment(element.createdAt, 'YYYY-MM-DD HH:mm:ss').format('DD MMM YYYY HH:mm:ss')} </ListItem.Subtitle>
            </ListItem.Content>
            <ListItem.Title style={{ paddingHorizontal: 12}}>
            { element.lap}
            </ListItem.Title>
          </ListItem>
        ))}
 
      </ScrollView>

    </SafeAreaView>

  );
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
  },
  container: {
    paddingTop: 20,
    paddingBottom: 12,
    backgroundColor: "#F4F5F4",
    flexDirection: "row",
    justifyContent: "space-between"
  },
  infoText: {
    fontSize: 16,
    color: "black",
    fontWeight: "bold",
    alignItems: "center",
    marginLeft: 20,
    marginRight: 20
  },
});

export default GameHistory;
