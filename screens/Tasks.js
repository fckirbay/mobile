import React, { useState } from "react";
import {
  StyleSheet,
} from "react-native";
import { WebView } from "react-native-webview";
import AsyncStorage from '@react-native-async-storage/async-storage';
import jwt_decode from "jwt-decode";

const Tasks = ({ navigation }) => {
  const [url, setUrl] = useState("");

  AsyncStorage.getItem('access_token')
      .then((value) => {
        const decoded = jwt_decode(value);
        setUrl("https://wall.adgaterewards.com/na6bpw/"+decoded.id);
  });

  return (
    url ? <WebView
      source={{ uri: url }}
    /> : null
  );
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
  },
});

export default Tasks;
