import React from "react";
import { View, StyleSheet, Text } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import i18n from 'i18n-js';

const PrivacyPolicy = ({ navigation }) => {
  return (
    <View>
      <View style={styles.container}>
        <Text style={styles.infoText}>{i18n.t('privacy_policy')}</Text>
      </View>
      <ScrollView>
      
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
  },
  container: {
    paddingTop: 20,
    paddingBottom: 12,
    backgroundColor: "#F4F5F4",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  infoText: {
    fontSize: 16,
    color: "black",
    fontWeight: "bold",
    marginLeft: 20,
    marginRight: 20,
  },
});

export default PrivacyPolicy;
