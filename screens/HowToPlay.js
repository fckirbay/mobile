import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text, StatusBar, SafeAreaView, Dimensions } from "react-native";
import { ListItem, Icon, Button, Image} from "react-native-elements";
import i18n from 'i18n-js';
import history from "../apis/History";
import moment from 'moment';

import games from "../apis/Games";
const { width, height } = Dimensions.get('window');

const HowToPlay = ({ navigation }) => {
    const [game, setGame] = useState([]);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
          async function getGames() {
            try {
                const { gameData } = await games({is_completed: 1});
                if(gameData.length > 0) {
                  setGame(gameData);
                }
            } catch (error) {
                console.log('Games getGames() Error', error);
            }
          }
          getGames();
        });
        return () => {
          unsubscribe();
        };
      }, [navigation]);

  
  return (
    <SafeAreaView>
        <View style={{ marginTop: 15, marginLeft: 10 }}>
            <Text style={styles.descriptionText}>Game Description</Text>
            <Text style={styles.subDetailText}> &rarr;  Guess if your card is higher or lower.</Text>
            <Text style={styles.subDetailText}> &rarr;  Skip 1 lap on each successful guess</Text>
            <Text style={styles.subDetailText}> &rarr;  If you guess wrong, it's game over.</Text>
            <Text style={styles.subDetailText}> &rarr;  Each game is played with 1 &#9733;</Text>
            <Text style={styles.subDetailText}> &rarr;  All the stars accumulated during the day can be seen on the game screen under the total reward.</Text>
            <Text style={styles.subDetailText}> &rarr;  At the end of the day, the WinWin user with the highest score wins all the accumulated stars.</Text>
            <Text style={styles.subDetailText}> &rarr;  Be the highest scorer, collect stars, withdraw your money.</Text>
            <Text style={styles.subDetailText}>  Good Luck!</Text>        
          </View>
          <View style={styles.gameLogo}>
          <Image 
            source={require('../assets/img/higherorlowertrans.png')} 
            style={styles.higherLowerImageTrans}
            />
        </View>
    </SafeAreaView>

  );
};

const styles = StyleSheet.create({
  subDetailText: {
    color: '#1c2247',
    margin: 2,
    fontSize: 16,
    fontWeight: '100',
    letterSpacing: 0.5,
    lineHeight: 28,
    fontWeight: "400",
    
  },
  descriptionText: {
    color: 'black',
    fontSize: 16,
    fontWeight: '400',
    letterSpacing: 1,
    marginBottom: 8,
    marginTop: 10,
    fontWeight: 'bold',
  },
  higherLowerImageTrans: {
    width: '60%',
    height: '60%',
    marginLeft: '20%',
    marginBottom: height * 0.1
  },
});

export default HowToPlay;
