import React, { useState, useEffect, useRef } from "react";
import { View, StyleSheet, Text, SafeAreaView, TouchableOpacity, Dimensions } from "react-native";
import { Divider, Icon, Image, Button } from "react-native-elements";
import CardFlip from 'react-native-card-flip';
import CountDown from 'react-native-countdown-component';

import games from "../apis/Games";
import gamelaps from "../apis/GameLaps";
import newgame from "../apis/NewGame";
import lessthan from "../apis/LessThan";
import greaterthan from "../apis/GreaterThan";
import continuegame from "../apis/ContinueGame";
import dailyreward from "../apis/DailyReward";
import bestscore from "../apis/BestScore";
import moment from "moment";
import momentTZ from "moment-timezone";
import i18n from 'i18n-js';

const { width, height } = Dimensions.get('window');

const FlipFlop = ({ navigation }) => {

  const [isPlaying, setIsPlaying] = useState(false);
  const [game, setGame] = useState([]);
  const [lap, setLap] = useState([]);
  const [lapStatus, setLapStatus] = useState(0);
  const [card2Status, setCard2Status] = useState(0);
  const [totalReward, setTotalReward] = useState(0);
  const [totalDuration, setTotalDuration] = useState(0);
  const [endOfDay, setEndOfDay] = useState(false);
  const [todayBest, setTodayBest] = useState(0);

  const card1 = useRef();
  const card2 = useRef();

  useEffect(() => {
    /*const unsubscribe = navigation.addListener('focus', () => {
      var date = moment(momentTZ().tz("Europe/Berlin")).format('YYYY-MM-DD hh:mm:ss');
      var expirydate = moment(momentTZ().tz("Europe/Berlin")).endOf('day').format('YYYY-MM-DD hh:mm:ss');
      var diffr = moment.duration(moment(expirydate).diff(moment(date)));
      //difference of the expiry date-time given and current date-time
      var hours = parseInt(diffr.asHours());
      var minutes = parseInt(diffr.minutes());
      var seconds = parseInt(diffr.seconds());
      var d = hours * 60 * 60 + minutes * 60 + seconds;
      setTotalDuration(d);
    });
    return () => {
      unsubscribe();
    };
    */
  }, []);
  
  /*const endOfDayFunc = () => {
    setTotalDuration(86400);
  }*/
  async function startGame() {
    const { newGameData } = await newgame();
    if(newGameData.error) {
      alert(newGameData.error);
    } else {
      setGame(newGameData.game);
      setLapStatus(newGameData.lap.result);
      if(newGameData.lap.result > 0) {
        card2.current.flip();
      }
      setLap(newGameData.lap);
      if(newGameData.lap == 2) {
        setTotalReward(1);
      } else {
        setTotalReward(Math.pow(2, (newGameData.lap - 2)))
      }
      setIsPlaying(true);
    }
  }

  async function play(type) {
    if(type == 'lower') {
      const { lessData } = await lessthan();

      console.log("lessData", lessData);
      setGame(lessData.game);
      setLap(lessData.lap);
      setLapStatus(lessData.lap.result);
      if(lessData.lap.result == 2) {
        setTotalReward(0);
      }
      card2.current.flip();
    } else {
      const { greaterData } = await greaterthan();

      console.log("greaterData", greaterData);
      setGame(greaterData.game);
      setLap(greaterData.lap);
      setLapStatus(greaterData.lap.result);
      if(greaterData.lap.result == 2) {
        setTotalReward(0);
      }
      card2.current.flip();
    }
  }

  async function continueGame() {
    setLapStatus(0);
    card1.current.flip();
    card2.current.flip();
    const { continueGameData } = await continuegame({id: game.id});
    setGame(continueGameData.game);
    setLap(continueGameData.lap);
    if(continueGameData.game.lap == 2) {
      setTotalReward(1);
    } else {
      setTotalReward(Math.pow(2, (continueGameData.game.lap - 2)))
    }
    card1.current.flip();
  }

  async function restartGame() {
    const { newGameData } = await newgame();
    if(newGameData.error) {
      alert(newGameData.error);
    } else {
      setLapStatus(0);
      card2.current.flip();
      setGame(newGameData.game);
      setLap(newGameData.lap);
      setIsPlaying(true);
      setTotalReward(0);
    }
  }

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      async function getGames() {
        try {
            const { gameData } = await games({is_completed: 0});
            if(gameData.length > 0) {
              setIsPlaying(true);
              setGame(gameData[0]);
              const { gameLapData } = await gamelaps({game_id: gameData[0].id});
              setLap(gameLapData[0]);
              setLapStatus(gameLapData[0].result);
              if(gameLapData[0].result > 0 && card2Status == 0) {
                setCard2Status(1);
                card2.current.flip();
              }
            } else {
              setIsPlaying(false);
              setLapStatus(0);
            }
            //const { dailyRewardData } = await dailyreward();
            //setTotalReward(dailyRewardData);
        } catch (error) {
            console.log('Games getGames() Error', error);
        }
      }
      async function getBestScore() {
        try {
            const { bestScoreData } = await bestscore();
            if(bestScoreData.data && bestScoreData.data != null) {
              setTodayBest(bestScoreData.data)
            } else {
              setTodayBest(0);
            }
        } catch (error) {
            console.log('Games getBestScore() Error', error);
        }
      }
      getGames();
      //getBestScore();
    });
    return () => {
      unsubscribe();
    };
  }, [navigation]);

  const renderStartScreen = () => {
    return <View style={{height: height}}>
      <View style={styles.gameLogo}>
        <Image 
          source={require('../assets/img/higherorlowertrans.png')} 
          style={styles.higherLowerImageTrans}
          />
      </View>
      {/*<View style={{ height: height * 0.1}}>
      <CountDown
          until={totalDuration}
          timeToShow={['H', 'M', 'S']}
          onFinish={() => endOfDayFunc()}
          size={16}
          showSeparator={true}
        />
    </View>*/}
    <View style={styles.mainButtonSection} >
      <View style={styles.startButtonSection}>
        <Button
          buttonStyle={styles.button3}
          titleStyle={{fontSize: 20}}
          onPress={() => navigation.navigate('GameHistory')}
          title={i18n.t('game_history')}
        />
        <Button
          buttonStyle={styles.button7}
          titleStyle={{fontSize: 20}}
          title={i18n.t('how_to_play')}
          onPress={() => navigation.navigate('HowToPlay')}
        />
      </View>
      <View style={styles.startButtonSection1}>
        {/*<Button
          buttonStyle={styles.button3}
          titleStyle={{fontSize: 20}}
          onPress={() => navigation.navigate('Winners')}
          title={i18n.t('winners')}
        />*/}
        <Button
          buttonStyle={styles.button4}
          titleStyle={{fontSize: 20}}
          title={i18n.t('new_game')}
          onPress={() => startGame()}
        />
        
      </View>
      </View>
    </View>
  }

  const renderGame = () => {
    return <View>
      <View style={styles.content}>
        <View style={styles.textWrapper}>
          <Text style={styles.statText}>{game ? game.lap : 1}</Text>
          <Text style={styles.statTextInfo}>{i18n.t('tour')}</Text>
        </View>
        <Divider orientation="vertical" width={1} />
        <View style={styles.textWrapper}>
          <View style={styles.subText}>
            <Text style={styles.statText}>{totalReward}<Icon
                  name="star"
                  size={15}
                  color="black"
                /></Text>
          </View>
          <Text style={styles.statTextInfo}>{i18n.t('total_reward')}</Text>
        </View>
        {/*<Divider orientation="vertical" width={1} />
        <View style={styles.textWrapper}>
          <View style={styles.subText}>
            <Text style={styles.statText}>{todayBest}</Text>
          </View>
          <Text style={styles.statTextInfo}>{i18n.t('today_best')}</Text>
        </View>*/}
      </View>
      <View style={{ flexDirection: 'row', justifyContent: 'center'}}>
        <CardFlip style={styles.cardContainer}  ref={card1}>
          <TouchableOpacity style={styles.card}>
            <Icon
              size={160}
              color="white"
              type="material-community"
              name={lap && lap.card_1 != null ? "numeric-" + `${lap.card_1}` : "numeric-0"}
            />
          </TouchableOpacity>
          <TouchableOpacity style={styles.card} >
            <Image 
              source={require('../assets/img/higherorlower.png')} 
              style={styles.higherLowerImage}
            />
          </TouchableOpacity>
        </CardFlip>
      </View>
      {lapStatus == 0 ? <View style={styles.buttonSection}>
        <Button
          buttonStyle={styles.button1}
          titleStyle={{fontSize: 20}}
          icon={
            <Icon
              type="font-awesome-5"
              name="angle-double-down"
              size={20}
              color="white"
              iconStyle={{ marginRight: 10}}
            />
          }
          title="Lower"
          onPress={() => play('lower')}
        />
        <Button
          buttonStyle={styles.button2}
          titleStyle={{fontSize: 20}}
          icon={
            <Icon
              type="font-awesome-5"
              name="angle-double-up"
              size={20}
              color="white"
              iconStyle={{ marginRight: 10}}
            />
          }
          title="Higher"
          onPress={() => play('higher')}
        />
      </View> : lapStatus == 1 ? <View style={styles.buttonSection}>
        <Button
          buttonStyle={styles.button5}
          titleStyle={{fontSize: 20}}
          icon={
            <Icon
              type="font-awesome-5"
              name="play"
              size={20}
              color="white"
              iconStyle={{ marginRight: 10}}
            />
          }
          title="Continue Game"
          onPress={() => continueGame()}
        />
      </View> : <View style={styles.buttonSection}>
        <Button
          buttonStyle={styles.button6}
          titleStyle={{fontSize: 20}}
          icon={
            <Icon
              type="material-community"
              name="refresh"
              size={24}
              color="white"
              iconStyle={{ marginRight: 10}}
            />
          }
          title="Restart Game"
          onPress={() => restartGame()}
        />
      </View>}
      <View style={{ flexDirection: 'row', justifyContent: 'center'}}>
        <CardFlip style={styles.cardContainer}  ref={card2} onFlipStart={(x) => setCard2Status(x)}>
          <TouchableOpacity style={styles.card} >
            <Image 
              source={require('../assets/img/higherorlower.png')} 
              style={styles.higherLowerImage}
            />
          </TouchableOpacity>
          <TouchableOpacity style={styles.card} >
            <Icon
              size={160}
              color="white"
              type="material-community"
              name={lap && lap.card_2 != null ? "numeric-" + `${lap.card_2}` : "numeric-0"}
            />
          </TouchableOpacity>
        </CardFlip>
      </View>
      {/*<View style={{ top: 20}}>
      <CountDown
          until={totalDuration}
          //duration of countdown in seconds
          timeToShow={['H', 'M', 'S']}
          //formate to show
          onFinish={() => endOfDayFunc()}
          //on Finish call
          showSeparator={true}
          //on Press call
          size={16}
        />
      </View>*/}
    </View>;
  }

  return (
    <SafeAreaView style={{height: height}}>
      { isPlaying ? renderGame() : renderStartScreen() }
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
  },
  container: {
    paddingTop: 20,
    paddingBottom: 12,
    backgroundColor: "#F4F5F4",
    flexDirection: "row",
    justifyContent: "space-between",
  },
  infoText: {
    fontSize: 16,
    color: "black",
    fontWeight: "bold",
    marginLeft: 20,
    marginRight: 20,
  },
  textWrapper: {
    width: '50%',
    marginTop: 10,
    marginBottom: 10,
  },
  statText: {
    fontSize: 22,
    fontWeight: "bold",
    alignSelf: "center",
    color: "#000000"
  },
  subStatText: {
    fontSize: 12,
    fontWeight: "bold",
    alignSelf: "center",
    marginTop: 10,
  },
  statTextInfo: {
    fontSize: 14,
    lineHeight: 30,
    alignSelf: 'center',
    color: "#2A6874"
  },
  content: {
    flexDirection: "row",
    marginTop: 20,
    marginBottom: 0,
    backgroundColor: '#e6e6e6',
    borderRadius: 10,
    marginLeft: '5%',
    width: '90%',
  },
  cardContainer: {
    marginTop: 20,
    width: width * 0.45,
    height: width * 0.45,
  },
  card: {
    height: '100%',
    backgroundColor: 'black',
    borderRadius: 5,
    shadowColor: 'rgba(0,0,0,0.5)',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.5,
    justifyContent: 'center',
  },
  higherLowerImage: {
    width: '100%',
    height: '100%',
    borderRadius: 10
  },
  higherLowerImageTrans: {
    width: '70%',
    height: '70%',
    marginLeft: '15%',
  },
  gameLogo: {
    height: height * 0.4,
    marginTop: height * 0.2,
    justifyContent: 'center',
  },
  buttonSection: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  mainButtonSection: {
    top: height * 0.25
  },
  startButtonSection: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
    bottom: height * 0.20,
  },
  startButtonSection1: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
    bottom: height * 0.18,
  },
  button1: {
    width: width * 0.4,
    height: width * 0.15,
    backgroundColor: '#C75555',
    fontSize: 20,
    borderRadius: 10,
    textTransform: "uppercase"
  },
  button2: {
    marginLeft: 30,
    width: width * 0.4,
    height: width * 0.15,
    backgroundColor: '#59AC51',
    fontSize: 20,
    borderRadius: 10
  },
  button3: {
    width: width * 0.4,
    height: width * 0.15,
    backgroundColor: 'grey',
    fontSize: 20,
    borderRadius: 10
  },
  button4: {
    marginLeft: 0,
    width: width * 0.9,
    height: width * 0.15,
    backgroundColor: '#59AC51',
    fontSize: 20,
    borderRadius: 10
  },
  button5: {
    width: width * 0.7,
    height: width * 0.15,
    backgroundColor: 'orange',
    fontSize: 20,
    borderRadius: 10
  },
  button6: {
    width: width * 0.7,
    height: width * 0.15,
    backgroundColor: 'red',
    fontSize: 20,
    borderRadius: 10
  },
  button7: {
    marginLeft: 30,
    width: width * 0.4,
    height: width * 0.15,
    backgroundColor: 'grey',
    fontSize: 20,
    borderRadius: 10
  },
});

export default FlipFlop;