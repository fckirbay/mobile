import axios from 'axios';

export default async function tasks(params) {
  try {

    const headers = { 'NO_LOADING': true, 'Content-Type': 'application/json' };

    const { data: response } = await axios("http://167.86.66.75:3001/api/offers", { method: 'GET', headers, params });

    //const { code, message } = response.result;

    const tasksData = response.data;
    
    return {
        tasksData,
    };
  } catch (error) {
    console.log(`tasks.get tasks Error`, error);
    return error;
  }
}
