import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default async function games(params) {
  try {
    const access_token = await AsyncStorage.getItem("access_token");

    const response = await axios.get('http://167.86.66.75:3001/api/games', { headers: { Authorization: `Bearer ${access_token}` }, params });

    //const { code, message } = response.result;

    const gameData = response.data.data;

    return {
        gameData,
    };
  } catch (error) {
    console.log(`games.get games Error`, error);
    return error;
  }
}
