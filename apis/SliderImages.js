import axios from 'axios';

export default async function sliderimages(params) {
  try {

    const headers = { 'NO_LOADING': true, 'Content-Type': 'application/json' };

    const { data: response } = await axios("http://167.86.66.75:3001/api/slider", { method: 'GET', headers, params });

    //const { code, message } = response.result;

    const sliderImagesData = response.data;
    
    return {
        sliderImagesData,
    };
  } catch (error) {
    console.log(`sliderImagesData.get sliderImagesData Error`, error);
    return error;
  }
}
