import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default async function userInfo() {
  try {
    
    const headers = { 'NO_LOADING': true, 'Content-Type': 'application/json' };

    const access_token = await AsyncStorage.getItem("access_token");

    const response = await axios.get('http://167.86.66.75:3001/api/users', { headers: { Authorization: `Bearer ${access_token}` } });

    //const { code, message } = response.result;
    
    const userData = response.data.data;

    return {
      userData,
    };
  } catch (error) {
    console.log(`userInfo.get userInfo Error`, error);
    return error;
  }
}
