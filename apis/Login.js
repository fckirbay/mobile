import axios from 'axios';

export default async function login(params) {
  try {

    const headers = { 'NO_LOADING': true, 'Content-Type': 'application/json' };

    const response = await axios.post('http://167.86.66.75:3001/api/auth/signin', params);

    //const { code, message } = response.result;

    const loginData = response.data;
        
    return {
        loginData,
    };
  } catch (error) {
    console.log(`loginData.post loginData Error`, error);
    return error;
  }
}
