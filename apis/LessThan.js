import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default async function lessthan(params = {}) {
  try {
    const access_token = await AsyncStorage.getItem("access_token");

    const response = await axios.post('http://167.86.66.75:3001/api/gamelap/lessthan', params, { headers: { Authorization: `Bearer ${access_token}` } });

    //const { code, message } = response.result;
    
    const lessData = response.data;

    return {
        lessData,
    };
  } catch (error) {
    console.log(`lessthan.get lessthan Error`, error);
    return error;
  }
}
