import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default async function greaterthan(params = {}) {
  try {
    const access_token = await AsyncStorage.getItem("access_token");

    const response = await axios.post('http://167.86.66.75:3001/api/gamelap/greaterthan', params, { headers: { Authorization: `Bearer ${access_token}` } });

    //const { code, message } = response.result;
    
    const greaterData = response.data;

    return {
        greaterData,
    };
  } catch (error) {
    console.log(`greaterthan.get greaterthan Error`, error);
    return error;
  }
}
