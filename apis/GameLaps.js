import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default async function gamelaps(params) {
  try {
    const access_token = await AsyncStorage.getItem("access_token");

    const response = await axios.get('http://167.86.66.75:3001/api/gamelaps', { headers: { Authorization: `Bearer ${access_token}` }, params });

    //const { code, message } = response.result;

    const gameLapData = response.data.data;

    return {
        gameLapData,
    };
  } catch (error) {
    console.log(`gamelaps.get gamelaps Error`, error);
    return error;
  }
}
