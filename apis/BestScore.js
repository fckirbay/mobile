import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default async function bestscore(params = {}) {
  try {
    const access_token = await AsyncStorage.getItem("access_token");

    const response = await axios.post('http://167.86.66.75:3001/api/user/bestscore', {}, { headers: { Authorization: `Bearer ${access_token}` } });

    //const { code, message } = response.result;
    
    const bestScoreData = response.data;

    return {
        bestScoreData,
    };
  } catch (error) {
    console.log(`bestscore.get bestscore Error`, error);
    return error;
  }
}
