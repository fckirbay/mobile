import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default async function history(params = '{}') {
  try {
    const access_token = await AsyncStorage.getItem("access_token");

    const response = await axios.get('http://167.86.66.75:3001/api/offerwall', { headers: { Authorization: `Bearer ${access_token}` }, params });

    //const { code, message } = response.result;

    const historyData = response.data.data;

    return {
        historyData,
    };
  } catch (error) {
    console.log(`history.get history Error`, error);
    return error;
  }
}
