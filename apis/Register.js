import axios from 'axios';

export default async function register(params) {
  try {

    const headers = { 'NO_LOADING': true, 'Content-Type': 'application/json' };

    const response = await axios.post('http://167.86.66.75:3001/api/auth/register', params);


    //const { data: response } = await axios("http://167.86.66.75:3001/api/auth/register", { method: 'POST', headers, params });

    //const { code, message } = response.result;

    const registerData = response.data;
        
    return {
        registerData,
    };
  } catch (error) {
    console.log(`registerData.post registerData Error`, error);
    return error;
  }
}
