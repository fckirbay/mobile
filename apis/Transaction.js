import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default async function transaction() {
  try {
    const access_token = await AsyncStorage.getItem("access_token");
    const headers = { 'NO_LOADING': true, 'Content-Type': 'application/json' };

    const response = await axios.get('http://167.86.66.75:3001/api/transactions', { headers: { Authorization: `Bearer ${access_token}` } });

    //const { code, message } = response.result;

    const transactionData = response.data.data;

    return {
        transactionData,
    };
  } catch (error) {
    console.log(`transaction.get transaction Error`, error);
    return error;
  }
}


