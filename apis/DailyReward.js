import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default async function dailyreward(params = '{}') {
  try {
    const access_token = await AsyncStorage.getItem("access_token");

    const response = await axios.get('http://167.86.66.75:3001/api/dailyreward', { headers: { Authorization: `Bearer ${access_token}` }, params });

    //const { code, message } = response.result;

    const dailyRewardData = response.data.data;

    return {
        dailyRewardData,
    };
  } catch (error) {
    console.log(`dailyreward.get dailyreward Error`, error);
    return error;
  }
}
