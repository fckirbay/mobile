import axios from 'axios';

export default async function generalstats(params) {
  try {

    const headers = { 'NO_LOADING': true, 'Content-Type': 'application/json' };

    const { data: response } = await axios("http://167.86.66.75:3001/api/stats", { method: 'GET', headers, params });

    //const { code, message } = response.result;

    const generalStatsData = response.data;
    
    return {
       generalStatsData,
    };
  } catch (error) {
    console.log(`generalstats.get generalstats Error`, error);
    return error;
  }
}
