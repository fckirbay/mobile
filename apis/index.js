import generalstats from './GeneralStats';
import sliderimages from './SliderImages';
import tasks from './Tasks';

module.exports = {
    generalstats,
    sliderimages,
    tasks
};
