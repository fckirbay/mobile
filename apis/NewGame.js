import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default async function newgame(params = {}) {
  try {
    const access_token = await AsyncStorage.getItem("access_token");

    const response = await axios.post('http://167.86.66.75:3001/api/newgame', params, { headers: { Authorization: `Bearer ${access_token}` } });

    //const { code, message } = response.result;
    
    const newGameData = response.data;

    return {
        newGameData,
    };
  } catch (error) {
    console.log(`newgame.get newgame Error`, error);
    return error;
  }
}
