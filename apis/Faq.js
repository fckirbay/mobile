import axios from 'axios';

export default async function faq() {
  try {

    const headers = { 'NO_LOADING': true, 'Content-Type': 'application/json' };

    const { data: response } = await axios("http://167.86.66.75:3001/api/faq?lang=EN", { method: 'GET', headers });

    //const { code, message } = response.result;

    const faqData = response.data;
    
    return {
        faqData,
    };
  } catch (error) {
    console.log(`faq.get faq Error`, error);
    return error;
  }
}
