import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default async function continuegame(params = {}) {
  try {
    const access_token = await AsyncStorage.getItem("access_token");

    const response = await axios.post('http://167.86.66.75:3001/api/gamelap/continuegame', params, { headers: { Authorization: `Bearer ${access_token}` } });

    //const { code, message } = response.result;
    
    const continueGameData = response.data;

    return {
        continueGameData,
    };
  } catch (error) {
    console.log(`continuegame.get continuegame Error`, error);
    return error;
  }
}
