import React from "react";
import * as Localization from 'expo-localization';
import i18n from 'i18n-js';
import Navigator from "./navigation/Navigator";
import en from './i18n/en.json'
import tr from './i18n/tr.json'

i18n.translations = {
  en,
  tr
};

i18n.locale = Localization.locale;
i18n.fallbacks = true;

const App = () => {
  return (
    <Navigator />
  );
};

export default App;
