import React, { useState } from 'react';
import { ListItem, Text } from "react-native-elements";

const Accordion = ({ title, content }) => {
  const [isActive, setIsActive] = useState(false);

  return (
    <ListItem.Accordion
    content={
      <>
        <ListItem.Content>
          <ListItem.Title style={{fontWeight: "bold"}}>{title}</ListItem.Title>
        </ListItem.Content>
      </>
    }
    isExpanded={isActive}
    onPress={() => setIsActive(!isActive)}
  >
      <ListItem  bottomDivider>
        <Text>{content}</Text>
      </ListItem>
  </ListItem.Accordion>

  );
};

export default Accordion;