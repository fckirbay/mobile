const theme = {
    light: {
        theme: "light",
        backColor: '#9cd6cf', //#181818
        headerColor: '#00FFFF',
        bottomTabColor: '#172A46',
        cardBackground: '#212121',
        textColor: 'white',
        statusColor: '#9cd6cf',
        toggleBackColor: '#2C2C2C',
        iconColor: '#ffffff', //#181818
        themeBack: '#C5EFF9'
    },
    dark: {
      theme: "dark",
      headerColor: '#00FFFF',
      bottomTabColor: 'white',
      cardBackground: 'white',
      textColor: 'black',
      statusColor: '#AF2235',
      toggleBackColor: '#FF055D',
      iconColor: '#ffffff',
      themeBack: '#fcf8f8'
    }
};

export default theme;
